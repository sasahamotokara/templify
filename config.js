import {fileURLToPath, URL} from 'node:url';
import {existsSync} from 'node:fs'
import {join} from 'node:path';
import {glob} from 'glob';

const cwd = process.cwd();
const cwfd = fileURLToPath(new URL(`file://${cwd}`));
const customConfigPath = new URL(`file://${cwd}/templify.config.js`);
const defaultTemplatePath = await glob('*.templify.js', {cwd: new URL('templates', import.meta.url)});

const [storybook, document, component] = await Promise.all(
    defaultTemplatePath.map((file) => import(new URL(`./templates/${file}`, import.meta.url)))
).then((templates) => templates.map(({default: fn}) => fn));

const configure = {
    componentDir: join(cwfd, './src/components'),
    storyDir: join(cwfd, './src/stories'),
    hierarchy: {
        1: '01_Atoms',
        2: '02_Molecules',
        3: '03_Organisms',
        4: '04_Templates',
        5: '05_Pages'
    },
    storybook,
    document,
    component,
};

if (existsSync(customConfigPath)) {
    const {default: customConfig} = await import(customConfigPath);

    Object.assign(configure, customConfig);
}

export default configure;
