export default function (ctx) {
    const {Name, level, dirname} = ctx;

    return `{/* ${Name}.mdx */}
import {Meta, Source, Story} from '@storybook/blocks';
import * as ${Name}Stories from './${Name}.stories';

# ${dirname}
<Meta of={${Name}Stories} />

コンポーネントの説明を記載

<Source code={'import {${Name}} from \'@/components/${level}/${dirname}\';'} />

## ${Name}

コンポーネントの説明を記載

<Story of={${Name}Stories.${Name}} />
`;
};
