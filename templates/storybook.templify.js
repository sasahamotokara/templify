export default function (ctx) {
    const {name, Name, naMe, level, dirname} = ctx;

    return `import {${Name} as ${naMe}} from '@/components/${level}/${dirname}';

export default {
    title: '${level}/${dirname}',
    component: ${naMe},
    argTypes: {}
};

export const ${Name} = {
    render: (_, {argTypes}) => ({
        components: {${naMe}},
        props: Object.keys(argTypes),
        template: '<${name} v-bind="$props" />'
    }),
    args: {}
};
`;
};
