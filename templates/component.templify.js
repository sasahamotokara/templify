export default function (ctx) {
    const {name, Name} = ctx;

    return `<template>
    <div class="${name}">${Name}</div>
</template>

<script setup>
defineProps({});
</script>

<script>
export default {
    inheritAttrs: false,
    customOptions: {}
};
</script>

<style lang="scss" scoped>
.${name} {
    font: inherit;
}
</style>
`;
};
