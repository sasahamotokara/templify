#!/usr/bin/env node

// import libraries.
import {existsSync, mkdirSync, appendFileSync, readFileSync, writeFileSync, createWriteStream} from 'node:fs';
import {join} from 'node:path';
import {Command} from 'commander';
import inflection from 'inflection';
import inquirer from 'inquirer';

// import configure.
import config from './config.js';

const program = new Command();

program
    .option('-y, --yes', 'Skip confirmation dialog', false)
    .option('-l, --level <number>', 'Number of hierarchy levels')
    .option('-d, --dirname <string>', 'Directory name')
    .option('-c, --component <string>', 'Component name')
    .option('--ignore-index', 'Skip generating or appending to index.js')
    .parse(process.argv);

const options = program.opts();
const questions = [{
        type: 'list',
        name: 'level',
        message: '作成するコンポーネントのレベルを指定してください。',
        choices: Object.values(config.hierarchy),
        default: Object.values(config.hierarchy).at(0),
        when: () => !options.level
    }, {
        type: 'input',
        name: 'dirname',
        message: 'コンポーネントの分類（サブディレクトリ名）を指定してください。',
        default: 'Components',
        when: () => !options.dirname
    }, {
        type: 'input',
        name: 'component',
        message: 'コンポーネントの名を指定してください。',
        default: 'Component',
        when: () => !options.component
    }
];
const {level, dirname, component} = await inquirer.prompt(questions).then((answers) => {
    const {dirname, component} = answers;

    // ディレクトリ名は複数系のキャメルケースに、コンポーネント名は単数形のキャメルケースに変換
    answers.dirname = inflection.pluralize(inflection.camelize(dirname.replaceAll(/\-| /g, '_')));
    answers.component = inflection.singularize(inflection.camelize(component.replaceAll(/\-| /g, '_')));

    return answers;
});
const {isContinueProcess = true} = await inquirer.prompt([{
    type: 'confirm',
    name: 'isContinueProcess',
    message: `内容に間違えがないか確認してください。

    ${level}/${dirname}/${component}.vue`,
    default: true,
    when: () => !options.yes
}]);

const createFile = (filepath, content) => {
    const writeStream = createWriteStream(filepath);

    writeStream.write(content);
    writeStream.end();
};

const createComponentFiles = (context) => {
    const componentDirname = join(config.componentDir, level, dirname);

    // ディレクトリが無ければ作成
    if (!existsSync(componentDirname)) {
        mkdirSync(componentDirname, {recursive: true});
    }

    // {component名}.vueファイルを作成
    createFile(join(componentDirname, `${component}.vue`), config.component(context));

    if (!options.ignoreIndex) {
        const indexFilePath = join(componentDirname, 'index.js');
        const exportSyntax = `export {default as ${component}} from './${component}.vue'; // eslint-disable-line\n`;

        // index.jsファイルが存在しない場合は作成する
        if (!existsSync(indexFilePath)) {
            createFile(indexFilePath, exportSyntax);

        // すでに存在する場合は、最終行に挿入
        } else {
            appendFileSync(indexFilePath, exportSyntax);

            // eslint無効コメントを削除
            writeFileSync(indexFilePath, readFileSync(indexFilePath, {encoding: 'utf8'}).replaceAll(' // eslint-disable-line', ''), 'utf8');
        }
    }
};

const createStoryFiles = (context) => {
    const storyDir = join(config.storyDir, level, dirname);
    const documentFilePath = join(storyDir, 'Document.mdx');

    // ディレクトリが無ければ作成
    if (!existsSync(storyDir)) {
        mkdirSync(storyDir, {recursive: true});
    }

    // {component名}.stories.jsファイルを作成
    createFile(join(storyDir, `${component}.stories.js`), config.storybook(context));

    // Document.mdxファイルが存在しない場合は作成する
    if (!existsSync(documentFilePath)) {
        createFile(documentFilePath, config.document(context));

    // すでに存在する場合
    } else {
        const documentFile = readFileSync(documentFilePath, {encoding: 'utf8'});
        const lastImportSyntax = [...documentFile.matchAll(/^import.+$/gm)].at(-1)[0];
        const importSyntax = `import * as ${component}Stories from './${component}.stories';`;

        // import文の最終行に、stories.jsのimport文を追加
        writeFileSync(documentFilePath, documentFile.replace(lastImportSyntax, `${lastImportSyntax}\n${importSyntax}`), 'utf8');

        // mdxの最終行にコンポーネントのセクションを追加
        appendFileSync(documentFilePath, `
## ${component}

コンポーネントの説明を記載

<Story of={${component}Stories.${component}} />
`);
    }
};

if (!isContinueProcess) {
    console.log('\nプロセスを中止しました。');
} else {
    const context = {
        name: inflection.dasherize(inflection.underscore(component)),
        Name: component,
        naMe: inflection.camelize(inflection.underscore(component), true),
        level,
        dirname
    };

    createComponentFiles(context);
    createStoryFiles(context);
    console.log('\nコンポーネントテンプレートを生成しました。');
}
